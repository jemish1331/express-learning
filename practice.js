async function getdata() {
  console.log("started");
  const picker = await "hello";
  console.log(picker);
  console.log("ended");
  return picker;
}
// getdata().then((res) => console.log(res + "hello"));
let promise = new Promise((resolve, reject) => {
  resolve("success");
  reject("err");
});
console.log("started");
promise.then(
  (res) => console.log(res),
  (err) => console.log(err)
);
console.log("ended");
